FROM rocker/tidyverse:4.1.0

# # Install R packages
# RUN install2.r --error \
#     methods \
#     jsonlite \
#     tseries

## install packages, java required for rJava - xlsx
RUN apt-get update \
	&& apt-get install --yes libcurl4-openssl-dev \
	&& apt-get install --yes libxml2-dev \
	&& apt-get install --yes libssl-dev \
	&& apt-get install --yes libcairo2-dev \
	&& apt-get install --yes libxt-dev \
	&& apt-get install --yes git \
	&& apt-get install -y default-jre  \
    && apt-get install -y default-jdk \
    && apt-get install -y ant \
	&& apt-get clean

RUN install2.r --error \
	enrichR \
	rJava \
	circlize \
	readr \
	dplyr \
	BiocManager  

RUN install2.r --error rmarkdown

RUN install2.r --error xlsx

RUN install2.r --error knitr

# required for 0.61.1 version in DESeq2
RUN Rscript -e "install.packages('matrixStats',repos='http://cran.us.r-project.org')"

RUN Rscript -e "BiocManager::install('ComplexHeatmap')"
RUN Rscript -e "BiocManager::install('DESeq2')"
RUN Rscript -e "BiocManager::install('GSVA')"
RUN Rscript -e "BiocManager::install('TCGAbiolinks')"

RUN R CMD javareconf

## create folder
RUN mkdir -p /auto-go

## copy auto-go source
COPY . /auto-go

## set main folder
WORKDIR /auto-go

# install R-packages
# RUN Rscript --vanilla /auto-go/libraries.R

# RUN Rscript -e "rmarkdown::render('proof_of_concept.Rmd')"


